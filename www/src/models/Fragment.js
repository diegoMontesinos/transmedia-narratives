/**
 * Fragment: a memory of the fest of Narrativas transmedia.
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Set debug ID
  var debug = require('debug')('fragment');

  // Brownian params
  var INTENSITY = 0.1;
  var FIDGETY   = 0.5;

  // Module definition
  var Fragment = function ( options ) {
    debug('Initialize with options: %o', options);

    this.id = options.id;

    this.time = this.parseTime(options.time);
    this.day  = options.day;
    this.date = new Date(2016, 10, this.day, this.time.hours, this.time.mins);

    this.duration = this.parseDuration(options.duration);

    this.thumb = document.createElement('img');
    this.thumb.src = 'assets/thumbs/' + this.id + '.png';

    var sourceWEBM  = document.createElement('source');
    sourceWEBM.src  = 'assets/videos/webm/' + this.id + '.webm';
    sourceWEBM.type = 'video/webm';

    var sourceMP4   = document.createElement('source');
    sourceMP4.src   = 'assets/videos/mp4/' + this.id + '.mp4';
    sourceMP4.type  = 'video/mp4';

    this.preview = document.createElement('video');
    this.preview.appendChild(sourceWEBM);
    this.preview.appendChild(sourceMP4);
    this.preview.loop = true;

    this.vimeo = options.vimeo;

    this.speaker = options.speaker;
    this.title = options.title;
    this.category = options.category;

    this.offsetY = options.offsetY || 0.0;

    var offsetMinutes = options.offsetMinutes || 0.0;
    this.offsetDate = new Date(this.date.getTime() + (offsetMinutes * 60 * 1000));

    this.brownian = [ 0, 0 ];
    this.walking = true;
  };

  Fragment.prototype = {

    parseTime: function ( timeStr ) {
      var parsed = timeStr.split(':');

      return {
        hours : parseInt(parsed[0] || 0),
        mins  : parseInt(parsed[1] || 0)
      };
    },

    parseDuration: function ( duration ) {
      var parsed = duration.split(':');

      var mins = parseInt(parsed[0] || 0);
      var secs = parseInt(parsed[1] || 0);

      return ((mins * 60) + secs) * 1000;
    },

    getDayStr: function () {
      return this.day + 'N';
    },

    getTimeStr: function () {
      var hoursStr = this.time.hours < 10 ? '0' + this.time.hours : '' + this.time.hours;
      var minsStr = this.time.mins < 10 ? '0' + this.time.mins : '' + this.time.mins;

      return hoursStr + ':' + minsStr + ' hrs.';
    },

    move: function () {
      this.position.x = this.walk(this.position.x, 0);
      this.position.y = this.walk(this.position.y, 1);
    },

    walk: function ( position, xy ) {
      var pseudoSwitch = Math.random() < FIDGETY ? -1 : 1;
      if (this.brownian[xy] !== 1 && this.brownian[xy] !== -1) {
        this.brownian[xy] = 1;
      }

      this.brownian[xy] *= pseudoSwitch;
      return position + INTENSITY * this.brownian[xy];
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  module.exports = Fragment;
});
