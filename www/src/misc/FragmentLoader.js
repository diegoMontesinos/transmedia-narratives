/**
 * Module to create the fragments from load the data of fragments
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Set debug ID
  var debug = require('debug')('misc:fragment-loader');

  // Dependencies
  var $ = require('jquery');

  // Models
  var Fragment = require('models/Fragment');

  // Module definition
  var FragmentLoader = function () {
    debug('Setup');
  };

  FragmentLoader.prototype = {

    // Load the data from a specified file path
    load: function ( filePath, callback ) {
      debug('Begin load...');

      this.onLoad = callback;

      $.ajax({
        url      : filePath,
        dataType : 'json',
        success  : this.onLoadData.bind(this)
      });
    },

    onLoadData: function ( data ) {
      var len = data.length;

      // Load each fragment
      this.fragments = [];

      data.forEach(function ( fragData ) {
        this.loadFragment(fragData, len);
      }, this);
    },

    loadFragment: function ( fragData, len ) {
      var fragment = new Fragment(fragData);
      this.fragments.push(fragment);

      if (this.fragments.length === len && this.onLoad) {
        debug('End load');

        this.onLoad(this.fragments);
      }
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Set module definition as exports
  module.exports = FragmentLoader;
});
