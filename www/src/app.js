/**
 * This module exports the singleton instance of the app "Narrativas transmedia".
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 *
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Set debug ID
  var debug = require('debug')('app');

  // Dependencies
  var $            = require('jquery');
  var MobileDetect = require('mobile-detect');

  var FragmentLoader = require('misc/FragmentLoader');
  var ViewController = require('views/ViewController');

  // Application definition
  var App = function () {
    debug('Setup app');

    var mobileDetect = new MobileDetect(window.navigator.userAgent);
    this.isMobile = mobileDetect.mobile();

    if (!this.isMobile) {
      this.loader = new FragmentLoader();

      this.viewController = new ViewController({
        width  : $('#app-content').width(),
        height : $('#app-content').height()
      });

      this.bindListeners();
    } else {
      $('#mobile-fallback').show();
    }
  };

  App.prototype = {

    /*****************
     * Setup methods *
     *****************/

    start: function () {
      debug('App has been started');

      if (!this.isMobile) {
        this.loader.load('data/fragments.json', this.onFragmentsLoad.bind(this));
      }
    },

    onFragmentsLoad: function ( fragments ) {
      this.viewController.start(fragments);
    },

    /*******************
     * Event Listeners *
     *******************/

    bindListeners: function () {
      $(window).resize(this.onWindowResize.bind(this));

      $(document).on('mousemove', (function ( evt ) {
        this.viewController.onMouseMove(evt);
      }).bind(this));
    },

    onWindowResize: function () {
      var w = $('#app-content').width();
      var h = $('#app-content').height();

      this.viewController.updateViewport(w, h);
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Return application instance as module export
  var app = new App();
  module.exports = app;
});
