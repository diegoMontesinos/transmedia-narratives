/**
 * Controller of the video show. Use Vimeo Player API.
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Set debug ID
  var debug = require('debug')('views:video-player');

  // Dependencies
  var $           = require('jquery');
  var VimeoPlayer = require('vimeo');

  var TRANSITION_DURATION = 350;

  // Module definition
  var VideoPlayer = function () {
    debug('Setup');

    this.setupPlayer();

    $('#fragment-show, #fragment-show-close').on('click', this.closeDialog.bind(this));
    $('#show-prev').on('click', function ( evt ) {
      evt.stopPropagation();
      evt.preventDefault();

      if (this.fragment && this.fragment.prev) {
        this.setFragment(this.fragment.prev, false);
      }
    }.bind(this));

    $('#show-next').on('click', function ( evt ) {
      evt.stopPropagation();
      evt.preventDefault();

      if (this.fragment && this.fragment.next) {
        this.setFragment(this.fragment.next, false);
      }
    }.bind(this));
  };

  VideoPlayer.prototype = {

    setupPlayer: function () {
      var options = {
        id       : 192582826,
        width    : 640,
        loop     : false
      };

      this.player = new VimeoPlayer('vimeo-video', options);
    },

    openDialog: function ( fragment, closeCallback ) {
      debug('Show video of fragment: %o', fragment);

      this.closeCallback = closeCallback;

      this.setFragment(fragment, true);
    },

    closeDialog: function () {

      this.player.getPaused()
      .then(function ( paused ) {
        if (!paused) {
          this.player.pause();
        }

        $('#fragment-show').fadeOut(TRANSITION_DURATION);

        if (this.closeCallback) {
          this.closeCallback();
        }
      }.bind(this))
      .catch(this.onVideoError);
    },

    onVideoError: function () {
      $('#fragment-show').fadeOut(TRANSITION_DURATION);
    },

    setFragment: function ( fragment, firstShow ) {
      this.fragment = fragment;

      if (!this.fragment.prev) {
        $('#show-prev').hide();
      } else {
        $('#show-prev').show();
      }

      if (!this.fragment.next) {
        $('#show-next').hide();
      } else {
        $('#show-next').show();
      }

      this.loadVideo(this.fragment.vimeo, firstShow);
    },

    loadVideo: function ( vimeoId, show ) {
      this.player.loadVideo(vimeoId)
      .then(function() {
        if (show) {
          $('#fragment-show').fadeIn(TRANSITION_DURATION);
        }

        this.player.play();
      }.bind(this));
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Set module definition as exports
  module.exports = VideoPlayer;
});
