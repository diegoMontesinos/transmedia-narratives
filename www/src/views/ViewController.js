/**
 * Main controller of all the things shown in the screen:
 *  - Timeline visualization
 *  - Vimeo Player
 *  - Menu
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  /* global FB */
  'use strict';

  // Set debug ID
  var debug = require('debug')('views:controller');

  // Dependencies
  var $           = require('jquery');

  var TimeLine    = require('views/TimeLine');
  var VideoPlayer = require('views/VideoPlayer');
  var STATES      = require('views/States');
  require('facebook');

  // Module definition
  var ViewController = function ( options ) {
    debug('Setup with options: %o', options);

    this.width  = options.width;
    this.height = options.height;

    this.setupSocialMedia();

    this.timeLine    = new TimeLine(options);
    this.videoPlayer = new VideoPlayer();

    this.state = STATES.GENERAL;
  };

  ViewController.prototype = {

    /****************
     * View methods *
     ****************/

    start: function ( fragments ) {
      debug('Starting view with fragments: %o', fragments);

      this.fragments = fragments;
      this.fragments.sort(function ( frag1, frag2 ) {
        return frag1.date.getTime() - frag2.date.getTime();
      });

      for (var i = 0, len = this.fragments.length; i < len; i++) {
        if (i > 0) {
          this.fragments[i].prev = this.fragments[i - 1];
        }

        if (i < (len - 1)) {
          this.fragments[i].next = this.fragments[i + 1];
        }
      }

      this.bindListeners();

      this.setupFragments();
      this.setupCategories();

      this.timeLine.setData(this.fragments, this.categories);

      this.updateViewport();
    },

    bindListeners: function () {
      $('li.nav-item.category').hover(function ( evt ) {
        $(evt.target).addClass('hover');
        this.onCategoryHover(evt);
      }.bind(this), function ( evt ) {
        $(evt.target).removeClass('hover');
      });

      $('li.nav-item.category').on('click', this.onCategoryClick.bind(this));

      $('#fragments-front').on('click', function () {
        if (this.state === STATES.CATEGORY) {
          this.deactivateCategory(this.activeCategory, this.activeFragment);

          this.state = STATES.GENERAL;
          $('#fragments-front').removeClass('clickable');
        }
      }.bind(this));
    },

    setupFragments: function () {
      this.fragments.forEach(function ( fragment, index ) {
        this.appendFragmentView(fragment, index);
        $('#fragments-back').append(fragment.$el);
      }, this);
    },

    setupCategories: function () {
      this.categories = {};

      this.fragments.forEach(function ( fragment ) {
        var category = fragment.category;
        if (!this.categories[category]) {
          this.categories[category] = [];
        }

        this.categories[category].push(fragment);
      }, this);
    },

    appendFragmentView: function ( fragment ) {
      var rootEl = $('<div class="fragment-container"></div>');
      rootEl.attr('fragment-id', fragment.id);

      var infoEl = $('<div class="fragment-info"></div>');
      infoEl.append('<div>' + fragment.speaker + '</div>');
      infoEl.append('<div>' + fragment.title + '</div>');
      infoEl.append('<div>' + fragment.getDayStr() + '</div>');
      infoEl.css({ opacity: 0.0 });

      var videoEl = $('<div class="fragment-video"></div>');
      videoEl.append(fragment.preview);
      videoEl.append(fragment.thumb);

      if (fragment.offsetY <= 0.0) {
        rootEl.append(infoEl);
        rootEl.append(videoEl);
      } else {
        rootEl.append(videoEl);
        rootEl.append(infoEl);
      }

      fragment.hasPreview = false;
      $(fragment.preview).hide();

      fragment.preview.oncanplay = function () {
        if (!fragment.hasPreview) {
          videoEl.find('img').remove();

          fragment.hasPreview = true;
          fragment.preview.currentTime = Math.floor(Math.random() * fragment.preview.duration);
          $(fragment.preview).show();
        }
      };

      fragment.$el = rootEl;
    },

    updateViewport: function ( width, height ) {
      this.width  = width  || this.width;
      this.height = height || this.height;

      this.timeLine.updateViewport(this.width, this.height);
    },

    setupSocialMedia: function () {

      FB.init({
        appId   : '206457453146294',
        xfbml   : true,
        version : 'v2.8'
      });

      $('#fb-share').on('click', function () {
        FB.ui({
          method  : 'share',
          display : 'popup',
          href    : 'http://lab22.canal22.org.mx/memoria-interactiva/',
        }, function() {});
      });

      var tweetOpts = {
        text : '1er Encuentro Internacional de Narrativas Documentales Transmedia',
        url  : 'http://lab22.canal22.org.mx/memoria-interactiva/'
      };

      var tweetHRef = 'https://twitter.com/intent/tweet?' + $.param(tweetOpts);
      $('#tweet-share-link').attr('href', tweetHRef);
    },

    /***********************
     * Interaction handler *
     ***********************/

    getFragmentById: function ( fragmentId ) {
      for (var i = 0; i < this.fragments.length; i++) {
        if (this.fragments[i].id === fragmentId) {
          return this.fragments[i];
        }
      }
    },

    activateFragment: function ( fragment, isRoot ) {
      fragment.walking = false;

      var x = parseFloat(fragment.$el.attr('data-x'));
      var y = parseFloat(fragment.$el.attr('data-y'));

      if (isRoot) {

        fragment.$el.find('.fragment-info').css({
          opacity: 1.0
        });

        fragment.$el.css({
          opacity : 1.0,
          zIndex  : 6,
          transform : 'translate(' + x + 'px, ' + y + 'px) scale(0.8, 0.8)'
        });
      } else {

        fragment.$el.find('.fragment-info').css({
          opacity: 0.3
        });

        fragment.$el.css({
          opacity   : 0.8,
          zIndex    : 4,
          transform : 'translate(' + x + 'px, ' + y + 'px) scale(0.8, 0.8)'
        });
      }

      fragment.$el.remove();
      $('#fragments-front').append(fragment.$el);

      // Since we remove the element, we need to add the click listener
      // to video
      var videoEl = fragment.$el.find('.fragment-video');
      videoEl.on('click', this.onFragmentClick.bind(this));
    },

    deactivateFragment: function ( fragment ) {
      fragment.walking = true;

      fragment.$el.find('.fragment-info').css({
        opacity: 0.0
      });

      var x = parseFloat(fragment.$el.attr('data-x'));
      var y = parseFloat(fragment.$el.attr('data-y'));

      fragment.$el.css({
        opacity   : 0.6,
        zIndex    : 0,
        transform : 'translate(' + x + 'px, ' + y + 'px) scale(0.7, 0.7)'
      });

      fragment.$el.remove();
      $('#fragments-back').append(fragment.$el);

      if (fragment.hasPreview) {
        fragment.preview.volume = 0.0;
        fragment.preview.pause();
      }
    },

    activateCategory: function ( category, rootFragment ) {
      $('.fragment-container').css({ opacity: 0.4 });

      category.forEach(function ( fragment ) {
        this.activateFragment(fragment, false);

        if (fragment.hasPreview) {
          fragment.preview.volume = 0.3;
          fragment.preview.play();
        }
      }, this);

      if (rootFragment) {
        this.activateFragment(rootFragment, true);

        if (rootFragment.hasPreview) {
          rootFragment.preview.volume = 1.0;
          rootFragment.preview.play();
        }
      }

      this.timeLine.activateCategory(category, rootFragment);
    },

    deactivateCategory: function ( category, rootFragment ) {
      $('.fragment-container').css({ opacity: 0.6 });

      category.forEach(function ( fragment ) {
        this.deactivateFragment(fragment);
      }, this);

      this.timeLine.deactivateCategory(category, rootFragment);
    },

    /******************
     * Events handler *
     ******************/

    onMouseMove: function ( evt ) {
      evt.stopPropagation();
      evt.preventDefault();

      $('ul.nav-list-2').css({
        display: ''
      });

      if (this.state !== STATES.GENERAL) {
        return;
      }

      var target = $(evt.target);

      if (target.hasClass('fragment-video')) {

        var fragId = target.parent().attr('fragment-id');

        if (this.activeFragment && this.activeFragment.id === fragId) {
          return;
        }

        if (this.activeFragment && this.activeFragment.id !== fragId) {
          this.deactivateCategory(this.activeCategory, this.activeFragment);
        }

        this.activeFragment = this.getFragmentById(fragId);
        this.activeCategory = this.categories[this.activeFragment.category];

        this.activateCategory(this.activeCategory, this.activeFragment);
      }
      else if (this.activeFragment && !target.hasClass('nav-item')) {
        this.deactivateCategory(this.activeCategory, this.activeFragment);

        delete this.activeCategory;
        delete this.activeFragment;
      }
    },

    onFragmentClick: function ( evt ) {
      var target = $(evt.target);

      // Get the fragment
      var fragId = target.parent().attr('fragment-id');
      var fragment = this.getFragmentById(fragId);

      if (this.state === STATES.CATEGORY) {
        this.activeCategory.forEach(function ( fragment ) {
          if (fragment.hasPreview) {
            fragment.preview.volume = 0;
            fragment.preview.pause();
          }
        });
      }

      if (this.state === STATES.GENERAL && this.activeFragment) {
        this.deactivateCategory(this.activeCategory, this.activeFragment);

        delete this.activeCategory;
        delete this.activeFragment;
      }

      // Show the video
      var lastState = this.state;
      this.state = STATES.VIDEO;

      this.videoPlayer.openDialog(fragment, function () {
        this.state = lastState;

        if (this.state === STATES.CATEGORY) {
          this.activeCategory.forEach(function ( fragment ) {
            if (fragment.hasPreview) {
              fragment.preview.volume = 0.3;
              fragment.preview.play();
            }
          });
        }
      }.bind(this));
    },

    onCategoryHover: function ( evt ) {
      var target = $(evt.target);

      var categoryId = target.attr('category-id');
      var category = this.categories[categoryId];

      if (this.activeCategory) {
        this.deactivateCategory(this.activeCategory, this.activeFragment);
      }

      this.activeFragment = category[Math.floor(Math.random() * category.length)];
      this.activeCategory = category;

      this.activateCategory(this.activeCategory, this.activeFragment);
    },

    onCategoryClick: function ( evt ) {
      var target = $(evt.target);
      target.removeClass('hover');
      target.parent().css({
        display: 'none'
      });

      $('#fragments-front').addClass('clickable');
      this.state = STATES.CATEGORY;
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Set module definition as exports
  module.exports = ViewController;
});
