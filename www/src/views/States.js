/**
 * States of visualization.
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Module definition
  var STATES = {
    GENERAL  : 0,
    CATEGORY : 1,
    VIDEO    : 2
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Set module definition as exports
  module.exports = STATES;
});
