/**
 * Module for control the timeline visualization of the fragments.
 *
 * @author Diego Montesinos [diegoa.montesinos@gmail.com]
 * ---
 * When I wrote this, only God and I understood what I was doing.
 * Now, God only knows.
 */
define(function (require, exports, module) {
  'use strict';

  // Set debug ID
  var debug = require('debug')('views:time-line');

  // Dependencies
  var $  = require('jquery');
  var d3 = require('d3');

  // Constants
  var BEGIN_DATE = new Date(2016, 10, 17, 9);
  var END_DATE   = new Date(2016, 10, 19, 21);

  var MAX_DURATION = 15 * 60 * 1000;

  var AXIS_OFFSET_AMT    = 0.05;
  var CONTENT_OFFSET_AMT = 0.055;

  var ACTIVATE_DURATION = 400;

  // Module definition
  var TimeLine = function ( options ) {
    debug('Setup with options: %o', options);

    this.width  = options.width;
    this.height = options.height;

    // Root element
    this.svg = d3.select('#timeline');

    // Create axis
    this.svg.append('g').attr('id', 'timeline-axis');

      this.svg.select('#timeline-axis')
        .append('rect')
          .attr('y', '0')
          .attr('height', '0.5');

      this.svg.select('#timeline-axis')
        .append('text')
          .attr('id', 'init-date')
          .text('17 N');

      this.svg.select('#timeline-axis')
        .append('text')
          .attr('id', 'end-date')
          .text('19 N');

    // Create content
    this.svg.append('g').attr('id', 'timeline-content');

    // Create scales
    this.offsetAxis    = this.width * AXIS_OFFSET_AMT;
    this.offsetContent = this.width * CONTENT_OFFSET_AMT;

    this.timeScale = d3.time.scale()
      .domain([ BEGIN_DATE, END_DATE ])
      .range([ this.offsetContent, this.width - (this.offsetContent * 2) ]);

    this.durationScale = d3.scale.linear()
      .domain([ 0, MAX_DURATION ])
      .range([ 0, this.width * 0.2 ]);

    this.movingNodes = false;
  };

  TimeLine.prototype = {

    /***************
     * Data Handle *
     ***************/

    setData: function ( fragments, categories ) {
      debug('Set data');

      this.data = {
        nodes : fragments,
        links : []
      };

      this.setNodes();

      this.setupLinksData(categories);
      this.setLinks();
    },

    updateNodesData: function () {
      this.data.nodes.forEach(function ( d ) {

        d.timelineX = this.timeScale(d.date);
        d.wDuration = this.durationScale(d.duration);

        var offsetDateX = this.timeScale(d.offsetDate);

        d.initPos = {
          x : offsetDateX + (d.wDuration * 0.5),
          y : (this.height * 0.5) + (this.height * d.offsetY)
        };

        d.position = {
          x : d.initPos.x,
          y : d.initPos.y
        };
      }, this);

      // Begin move nodes
      if (!this.movingNodes) {
        this.movingNodes = true;
        this.moveNodes();
      }
    },

    setupLinksData: function ( categories ) {
      $.each(categories, (function ( category, members ) {
        for (var i = 0; i < members.length; i++) {
          var source = members[i];
          for (var j = (i + 1); j < members.length; j++) {
            var end = members[j];

            var link = {
              category : category,
              source   : source,
              end      : end
            };

            this.data.links.push(link);
          }
        }
      }).bind(this));
    },

    /*****************
     * Setup Methods *
     *****************/

    setNodes: function () {
      debug('Set nodes');

      // Set data and create
      this.nodes = this.svg.select('#timeline-content')
        .selectAll('.node')
          .data(this.data.nodes);

      this.nodes.enter()
        .append('g')
          .attr('class', 'node')
          .attr('frag-id', function ( d ) { return d.id; });

      // Duration
      this.nodes
        .append('rect')
          .attr('class', 'duration');

      // Time mark
      this.nodes
        .append('g')
          .attr('class', 'time-mark');

      this.nodes.selectAll('.time-mark')
        .append('circle')
          .attr('cx', 0).attr('cy', 0).attr('r', 3);

      // Fragment content (all magic happens with this object)
      this.nodes.append('g')
        .attr('class', 'fragment');

      this.nodes.selectAll('.fragment')
        .append('rect')
          .attr('class', 'fragment-frame');

      this.nodes.selectAll('.fragment')
        .append('circle')
          .attr('cx', 0)
          .attr('cy', 0)
          .attr('r', 2);
    },

    setLinks: function () {
      debug('Set links');

      // Set data and create
      this.links = this.svg.select('#timeline-content')
        .selectAll('.link')
          .data(this.data.links);

      this.links.enter()
        .append('line')
          .attr('class', 'link')
          .attr('category', function ( d ) { return d.category; })
          .attr('source-id', function ( d ) { return d.source.id; })
          .attr('end-id', function ( d ) { return d.end.id; });
    },

    /***************
     * Update view *
     ***************/

    updateViewport: function ( width, height ) {

      this.width  = width;
      this.height = height;

      this.offsetAxis    = this.width * AXIS_OFFSET_AMT;
      this.offsetContent = this.width * CONTENT_OFFSET_AMT;

      // Update svg
      this.svg.attr('width', this.width).attr('height', this.height);

      // Update scales
      this.timeScale.range([ this.offsetContent, this.width - (this.offsetContent * 2) ]);
      this.durationScale.range([ 0, this.width * 0.2 ]);

      this.updateAxis();

      this.svg.select('#timeline-content')
        .attr('transform', 'translate(' + this.offsetContent + ', 0)');

      if (this.nodes) {
        this.updateNodes();
      }
    },

    updateAxis: function () {

      this.svg.select('#timeline-axis')
        .attr('transform', 'translate(0, ' + (this.height / 2.0) + ')');

      this.svg.select('#timeline-axis rect')
        .attr('x', this.offsetAxis)
        .attr('width', this.width - (this.offsetAxis * 2));

      this.svg.select('#init-date')
        .attr('x', this.offsetAxis)
        .attr('y', '18');

      this.svg.select('#end-date')
        .attr('x', this.width - this.offsetAxis)
        .attr('y', '18');
    },

    updateNodes: function () {

      // Update and bind data
      this.updateNodesData();
      this.nodes = this.svg.selectAll('.node')
        .data(this.data.nodes);

      var halfH = this.height * 0.5;

      // Duration
      this.nodes.selectAll('.duration')
        .attr('x', function ( d ) {
          return d.timelineX - (d.wDuration * 0.5);
        })
        .attr('y', halfH - 4)
        .attr('width', '0')
        .attr('height', 8);

      // Time position
      this.nodes.selectAll('.time-mark')
        .attr('transform', function ( d ) {
          var x = d.timelineX;
          return 'translate(' + x + ',' + halfH + ')';
        });

      this.nodes.selectAll('.time-mark')
        .select('circle:nth-child(1)')
        .style('opacity', 0.0);

      // Translate fragment
      this.nodes.selectAll('.fragment')
        .style('transform', function ( d ) {
          return 'translate(' + d.position.x + 'px, ' + d.position.y + 'px)';
        });

      this.nodes.selectAll('.fragment circle')
        .style('opacity', 0);

      this.data.nodes.forEach(this.updateNodePosition.bind(this));
    },

    updateNodePosition: function ( d ) {
      var el = d.$el;
      el.css({ transform: '' });

      var w = el.width();

      var x = d.position.x + this.offsetContent - (w * 0.5);
      var y = d.position.y - (el.find('.fragment-video').height() * 0.5);

      if (d.offsetY <= 0.0) {
        y -= el.find('.fragment-info').height() + 4;
      }

      el.attr('data-x', x);
      el.attr('data-y', y);
      if (d.walking) {
        el.css({
          transform : 'translate(' + x + 'px, ' + y + 'px) scale(0.7, 0.7)'
        });
      } else {
        el.css({
          transform : 'translate(' + x + 'px, ' + y + 'px) scale(0.8, 0.8)'
        });
      }
    },

    moveNodes: function() {
      this.data.nodes.forEach(function ( d ) {
        if (d.walking) {
          d.move();
          this.updateNodePosition(d);
        }
      }, this);

      requestAnimationFrame(this.moveNodes.bind(this));
    },

    /**********************
     * Active interaction *
     **********************/

    activateFragment: function ( fragment ) {
      fragment.position = {
        x : fragment.initPos.x,
        y : fragment.initPos.y
      };
      this.updateNodePosition(fragment);

      var fragNode = this.svg.select('.node[frag-id="' + fragment.id + '"]');

      fragNode.select('.duration')
        .attr('width', '0')
      .transition()
      .duration(ACTIVATE_DURATION)
        .attr('width', function ( d ) { return d.wDuration; });

      fragNode.select('.fragment circle')
        .style('opacity', 0.0)
      .transition()
      .duration(ACTIVATE_DURATION)
        .style('opacity', 1.0);

      fragNode.select('.time-mark circle:nth-child(1)')
        .style('opacity', 0.0)
      .transition()
      .duration(ACTIVATE_DURATION)
        .style('opacity', 1.0);
    },

    deactivateFragment: function ( fragment ) {
      var fragNode = this.svg.select('.node[frag-id="' + fragment.id + '"]');

      fragNode.select('.duration')
        .attr('width', function ( d ) { return d.wDuration; })
      .transition()
      .duration(ACTIVATE_DURATION)
        .attr('width', '0');

      fragNode.select('.fragment circle')
        .style('opacity', 1.0)
      .transition()
      .duration(ACTIVATE_DURATION)
        .style('opacity', 0.0);

      fragNode.select('.time-mark circle:nth-child(1)')
        .style('opacity', 1.0)
      .transition()
      .duration(ACTIVATE_DURATION)
        .style('opacity', 0.0);
    },

    getInitLine: function ( d, rootFragment ) {
      if (d.source.id === rootFragment.id || d.end.id === rootFragment.id) {
        return rootFragment.position;
      }

      return { x: 0, y: 0 };
    },

    getEndLine: function ( d, rootFragment ) {
      if (d.source.id === rootFragment.id) {
        return d.end.position;
      }

      if (d.end.id === rootFragment.id) {
        return d.source.position;
      }

      return { x: 0, y: 0 };
    },

    activateCategory: function ( category, rootFragment ) {

      if (!rootFragment) {
        rootFragment = category[0];
      }

      category.forEach(function ( fragment ) {
        this.activateFragment(fragment);
      }, this);

      this.svg.selectAll('.link[category="' + rootFragment.category + '"]')
        .style('display', function ( d ) {
          if (d.source.id === rootFragment.id || d.end.id === rootFragment.id) {
            return 'initial';
          } else {
            return 'none';
          }
        })
        .attr('x1', (function ( d ) { return this.getInitLine(d, rootFragment).x; }).bind(this))
        .attr('y1', (function ( d ) { return this.getInitLine(d, rootFragment).y; }).bind(this))
        .attr('x2', (function ( d ) { return this.getInitLine(d, rootFragment).x; }).bind(this))
        .attr('y2', (function ( d ) { return this.getInitLine(d, rootFragment).y; }).bind(this))
      .transition()
      .duration(ACTIVATE_DURATION)
        .attr('x2', (function ( d ) { return this.getEndLine(d, rootFragment).x; }).bind(this))
        .attr('y2', (function ( d ) { return this.getEndLine(d, rootFragment).y; }).bind(this));
    },

    deactivateCategory: function ( category, rootFragment ) {

      if (!rootFragment) {
        rootFragment = category[0];
      }

      category.forEach(function ( fragment ) {
        this.deactivateFragment(fragment);
      }, this);

      this.svg.selectAll('.link[category="' + rootFragment.category + '"]')
        .style('display', function ( d ) {
          if (d.source.id === rootFragment.id || d.end.id === rootFragment.id) {
            return 'initial';
          } else {
            return 'none';
          }
        })
        .attr('x1', (function ( d ) { return this.getInitLine(d, rootFragment).x; }).bind(this))
        .attr('y1', (function ( d ) { return this.getInitLine(d, rootFragment).y; }).bind(this))
        .attr('x2', (function ( d ) { return this.getEndLine(d, rootFragment).x; }).bind(this))
        .attr('y2', (function ( d ) { return this.getEndLine(d, rootFragment).y; }).bind(this))
      .transition()
      .duration(ACTIVATE_DURATION)
        .attr('x2', (function ( d ) { return this.getInitLine(d, rootFragment).x; }).bind(this))
        .attr('y2', (function ( d ) { return this.getInitLine(d, rootFragment).y; }).bind(this));
    }
  };

  // Simple exports check
  if (!exports) {
    exports = {};
  }

  // Set module definition as exports
  module.exports = TimeLine;
});
