/* globals requirejs */

// RequireJS configuration
requirejs.config({

  baseURL: 'src',

  // Path shortcuts for common used components
  paths: {

    // Utils
    'jquery'        : 'lib/jquery/jquery',
    'debug'         : 'lib/utils/debug',
    'mobile-detect' : 'lib/utils/mobile-detect',
    'vimeo'         : [ '//player.vimeo.com/api/player', 'lib/vimeo/player' ],

    // d3
    'd3'            : 'lib/d3/d3',

    // Social media
    'facebook'      : '//connect.facebook.net/en_US/sdk'
  },

  // Load configuration options for non-module libraries
  shim: {
    'facebook'      : { exports: 'FB' }
  }
});

// Load main application file
requirejs([ 'jquery', 'app' ], function ( $, app ) {
  'use strict';

  $(document).ready(function () {
    app.start();
  });
});
